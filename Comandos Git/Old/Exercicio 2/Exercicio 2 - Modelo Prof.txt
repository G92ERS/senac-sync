<script>

    const nota1 = 11;
    const nota2 = 10;
    const nota3 = 9;
    
    const media = (nota1 + nota2 + nota3) / 3;

    if(media > 10)
        console.log("Entre em contato com a Diretoria!");        
    else if(media == 10)
        console.log("Aprovado com Louvor!");
    else if(media >= 7)
        console.log("Aprovado!");
    else
        console.log("Reprovado!");

</script>
