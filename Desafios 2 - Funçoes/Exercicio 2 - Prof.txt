    <script>        
        function centrimetrosEmPolegadas(centimetros) {
            return centimetros * 0.39;
        }

        function polegadasEmCentimetros(polegadas) {
            return polegadas * 2.54;
        }

        const polegadas = centrimetrosEmPolegadas(1456);
        console.log(`Cm em in: ${polegadas}`);

        const centimetros = polegadasEmCentimetros(14453);
        console.log(`In em cm: ${centimetros}`);
    </script>